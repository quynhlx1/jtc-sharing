import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'member'
})
export class MemberPipe implements PipeTransform {

  transform(value: string[], args?: any): any {
    return value ? (value.length === 0 ? 'Only You' : (value.length === 1 ? `1 member` : `${value.length} members`)) : '';
  }

}
