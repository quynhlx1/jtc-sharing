import { IFile } from './../../../models/IFile';
import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit, OnChanges {
  @Input() routes: IFile[] = [];
  lastRoute: IFile;

  constructor(
  ) {
  }

  ngOnInit() {
  }

  ngOnChanges() {
    this.lastRoute = this.routes[this.routes.length - 1];
  }

}
