import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoCompletedComponent } from './auto-completed.component';

describe('AutoCompletedComponent', () => {
  let component: AutoCompletedComponent;
  let fixture: ComponentFixture<AutoCompletedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoCompletedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoCompletedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
