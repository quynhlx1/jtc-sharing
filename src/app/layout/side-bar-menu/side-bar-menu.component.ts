import { IMenu } from './../../models/IMenu';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-side-bar-menu',
  templateUrl: './side-bar-menu.component.html',
  styleUrls: ['./side-bar-menu.component.scss']
})
export class SideBarMenuComponent implements OnInit {
  menus: [IMenu];
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    this.menus = [
      { routerLink: 'home', name: 'My files', active: true },
      { routerLink: 'sharing', name: 'Sharing', active: false },
      { routerLink: 'request', name: 'File requests', active: false },
      { routerLink: 'deleted', name: 'Deleted files', active: false },
    ];
  }

  selectMenu(menu: IMenu) {
    for (const m of this.menus) {
      m.active = false;
    }
    menu.active = true;

    this.router.navigate([menu.routerLink]);
  }

}

