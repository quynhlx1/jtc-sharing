import { IFile } from './../../../models/IFile';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-dialog-delete',
    templateUrl: 'dialog-delete.html'
})

export class DialogDeleteComponent {
    constructor(
        public dialogRef: MatDialogRef<DialogDeleteComponent>,
        @Inject(MAT_DIALOG_DATA) public file: IFile
    ) { }

    onNoClick(): void {
        this.dialogRef.close();
    }

    done() {
        this.file.isDeleted = true;
        this.dialogRef.close(this.file);
    }
}
