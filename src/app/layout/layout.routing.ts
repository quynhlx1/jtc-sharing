import { SharingComponent } from './sharing/sharing.component';
import { LayoutComponent } from './layout.component';
import { MyFilesComponent } from './my-files/my-files.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchComponent } from './search/search.component';

const routes: Routes = [
    {
        path: '', component: LayoutComponent, children: [
            { path: 'sharing', component: SharingComponent},
            { path: 'search', component: MyFilesComponent},
            { path: 'home', component: MyFilesComponent },
            { path: 'home/:id', component: MyFilesComponent },
            { path: '', redirectTo: 'home', pathMatch: 'full' }
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LayoutRoutingModule { }
