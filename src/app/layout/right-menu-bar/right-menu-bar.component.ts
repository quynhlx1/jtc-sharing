import { IFile } from './../../models/IFile';
import { environment } from './../../../environments/environment.prod';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy, Output, Input, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { FileUploader, FileUploaderOptions, FileItem } from 'ng2-file-upload';
import { FileStore } from '../../states/file.state';
import { FileType } from '../../models/IFile';

@Component({
  selector: 'app-right-menu-bar',
  templateUrl: './right-menu-bar.component.html',
  styleUrls: ['./right-menu-bar.component.scss']
})
export class RightMenuBarComponent implements OnInit, OnDestroy {
  @Output() onNewFolder = new EventEmitter();
  @Input() fileUploading: FileItem;
  @Input() selectedFiles: IFile[];
  @Output() onDownload = new EventEmitter<IFile>();
  @Output() onRename= new EventEmitter<IFile>();

  @ViewChild('uploadInput') private uploadElementRef: ElementRef;
  uploader: FileUploader = new FileUploader({ url: '/api/file/upload' });
  sub: any;
  constructor(
    private route: ActivatedRoute,
    private fileStore: FileStore
  ) {


  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const options: FileUploaderOptions = { url: `${environment.apiUrl}/api/file/upload`, additionalParameter: { parentId: params.id }, };
      this.uploader = new FileUploader(options);
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  createNewFolder() {
    this.onNewFolder.emit();
  }

  onUploadClick() {
    this.uploadElementRef.nativeElement.click();
  }

  upload() {
    this.uploader.onBeforeUploadItem = (item) => {
      item.withCredentials = false;
      this.fileUploading = item;
      item.onProgress = (number) => {
        console.log(number);
      };
    };
    this.uploader.uploadAll();
    this.uploader.onSuccessItem = (item, response, status, headers) => {
      const fileRaw = JSON.parse(response);
      fileRaw.type = fileRaw['isFile'] ? fileRaw.format : FileType.folder;
      fileRaw.id = fileRaw['_id'];
      this.fileStore.addFile(fileRaw);
    };
  }

  download(file: IFile) {
    this.onDownload.emit(file);
  }

  rename(file: IFile) {
    this.onRename.emit(file);
  }
}
