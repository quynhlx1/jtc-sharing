
export interface IMenu {
    routerLink?: string;
    name: string;
    active: Boolean;
}