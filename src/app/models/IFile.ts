export interface IFile {
    id: string;
    name?: string;
    type?: FileType;
    modified?: Date;
    members?: string[];
    checked?: Boolean;
    isEdit?: Boolean;
    isDeleted?: Boolean;
    parent?: string;
  }
  export enum FileType {
    folder = 'folder',
    file = 'file',
    docx = 'docx',
    doc = 'doc',
    xlsx = 'xlsx',
    xls = 'xls',
    pdf = 'pdf',
    ppt = 'ppt'
  }
